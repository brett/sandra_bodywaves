#!/usr/bin/env python
#Henry Brett, Utrecht University, 2018
import glob
import obspy
from obspy import read, UTCDateTime, read_inventory, read_events
from obspy.taup import TauPyModel
from obspy.geodetics import locations2degrees
from obspy.clients.fdsn import Client

#######################
#Read in Data
def ReadDataStationEvent_function() :
    mseedfilename=glob.glob("*.mseed")
    Data = read(mseedfilename[0])
    #Store Trace as an individual object
    trace=Data[0]
    network=trace.stats.network
    station=trace.stats.station
    starttime=trace.stats.starttime
    test="test"
######################
    inventoryname=str(trace.stats.network)+"."+str(trace.stats.station)+".xml"
    inv=read_inventory(inventoryname)
    net=inv[0]
    stalat=net[0].latitude
    stalon=net[0].longitude
#######################
    cat=read_events("EventInfo.xml")
    EventtimeUSGS=cat.events[0].origins[0].time
    Evlon=cat.events[0].origins[0].longitude
    Evlat=cat.events[0].origins[0].latitude
    depth=cat.events[0].origins[0].depth/1000
    Magnitude=cat.events[0].magnitudes[0].mag
    return(Data,trace,network,station,starttime,net,stalat,stalon,EventtimeUSGS,Evlon,Evlat,depth,Magnitude)
    print(EventtimeUSGS)
if __name__ == '__main__':
    print("###############################")
    print("Read in Event, Station and Data")
    print("###############################")
    Data,trace,network,station,starttime,net,stalat,stalon,EventtimeUSGS,Evlon,Evlat,depth,Magnitude=ReadDataStationEvent()