#############################################################################################
##########################   BODY WAVES MODULE: DATA PROCESS   ##############################
#############################################################################################
#Henry Brett, Utrecht University, 2018,2019,2020
#This is one of my first python codes so not everything about it is pretty - although i have cleaned it up significantly since then.
#For a given folder containing a trace, event.xml and station.xml file load in the seismogram,
#De trend the seismogram, filter it, predict arrivals of PKPdf, PKPab and PKPbc and open a window
#to allow for picking of arrivals. Once the arrivals have been picked it will then produce a results.xlsx 
#which is a spreadsheet containing pick and Zeta information
### Import Packages ###
#Obspy:
import obspy
from obspy import read, UTCDateTime, read_inventory, read_events
from obspy.taup import TauPyModel
from obspy.geodetics import locations2degrees,gps2dist_azimuth
#Other:
from geographiclib.geodesic import Geodesic 
import sys
import glob
import pandas as pd
from math import pi,cos,sin,acos,asin,sqrt,atan2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.dates import date2num
import os
import matplotlib.gridspec as gridspec
#Personal:
from Data_Read import ReadDataStationEvent_function

def ZetaCalculate(Inlat,Inlon,Outlat,Outlon):
    Inlat_co_r=(90-Inlat)*(pi/180)      #colattitude in radians
    Inlon_r=Inlon*(pi/180)              #longitude   in radians
    Outlat_co_r=(90-Outlat)*(pi/180)    #colattitude in radians
    Outlon_r=Outlon*(pi/180)            #longitude   in radians
    #Conduct calculation, Numerator:
    top=(cos(Outlat_co_r)-cos(Inlat_co_r))
    lower1=2-2*cos(Outlat_co_r)*cos(Inlat_co_r)
    lower2=2*sin(Outlat_co_r)*sin(Inlat_co_r)*cos(Inlon_r-Outlon_r)
    bottom=sqrt(lower1-lower2)
    zeta_r=acos(top/bottom)
    zeta=zeta_r*(180/pi)
    if zeta > 90 :
        zeta=180-zeta    
    return zeta

def Data_Process() :
    print("####################################################################") 
    #Phase Switches - will change if TauP predicts their arrival:
    PKPbc_switch=False
    PKPab_switch=False
    
    #####################################################################################
    # Data Read and Filter
    ##################################################################################### 
    #Read in Data,Station and Event Info:
    Data,trace,network,station,starttime,net,stalat,stalon,EventtimeUSGS,Evlon,Evlat,depth,Magnitude=ReadDataStationEvent_function()
     
    #Filter Data:
    trace_filt=trace.copy()
    trace_filt.detrend(type='demean')
    trace_filt.detrend(type='linear')
    trace_filt.filter("bandpass",freqmin=0.5,freqmax=2,corners=2)
     
    #Azimuth and Distance from event to station
    dist=locations2degrees(lat1=Evlat,long1=Evlon,lat2=stalat,long2=stalon)
    azimuth_temp=gps2dist_azimuth(Evlat,Evlon,stalat,stalon)
    azimuth=azimuth_temp[1]
    
    #Setup branches to do with whether PKPab and PkPbc are visible
    if dist > 146.5 and dist < 155.5:
        PKPbc_switch=True
    if dist > 146.5 and dist < 178:
        PKPab_switch=True
    
    #####################################################################################
    # TauP: Calculate Phase arrivals
    ##################################################################################### 
    #Calculate Arrival times from TauP
    taupmodel="AK135"
    model=TauPyModel(model=taupmodel)
    phases=["PKIKP","PKP"]
    arrivals= model.get_travel_times(source_depth_in_km=depth,distance_in_degree=dist,phase_list=phases)
    arrivals= model.get_ray_paths(source_depth_in_km=depth, distance_in_degree=dist, phase_list=phases)
    arrivals= model.get_pierce_points(depth,dist,phase_list=phases)
    arrivaltimelength=len(arrivals)

    #check which phase is which using pierce points, max depth of PKIKP>PKPbc>PKPab
    maxdepths=[]
    for i in range(0,len(arrivals)):
        depths_temp=[]
        pierce=arrivals[i].pierce
        for j in range(0,len(pierce)):
            depths_temp.append(pierce[j][3])
        maxdepths.append(max(depths_temp))

    if arrivaltimelength == 1:
        PKIKP_index=0
    if arrivaltimelength == 3:
        PKIKP_index=np.argwhere(np.array(maxdepths)>5154)
        PKIKP_index=PKIKP_index[0][0]
        maxdepths[PKIKP_index]=-999
        PKPbc_index=np.argmax(maxdepths)
        maxdepths[PKPbc_index]=-999
        PKPab_index=np.argmax(maxdepths)
    if arrivaltimelength == 2:
        PKIKP_index=np.argwhere(np.array(maxdepths)>5154)
        PKIKP_index=PKIKP_index[0][0]
        maxdepths[PKIKP_index]=-999
        PKPab_index=np.argmax(maxdepths)

    print('######Distance######')
    print('No. of arrivals:',arrivaltimelength) 
    print('Distance:',dist)
    print(arrivals)
    print(PKPab_switch,PKPbc_switch)
    print('######Distance######')
    #Check if indexing is correct:
    if (arrivals[PKIKP_index].purist_name !="PKIKP"):
        print('Error: TauP indexing incorrect. Exit.')
        sys.exit()
    #Check if a cd phsae has occured?    
    if arrivaltimelength>3:
        print("Error: cd phase detected. Exit.")
        sys.exit()    
    
    #All Three Arrivals
    if arrivaltimelength == 3 and (arrivals[PKIKP_index].purist_name=="PKIKP"):
        PKIKP_taup=starttime+arrivals[PKIKP_index].time
        PKPbc_taup=starttime+arrivals[PKPbc_index].time
        PKPab_taup=starttime+arrivals[PKPab_index].time
        PKIKP_taup_seconds=PKIKP_taup-starttime
        PKPbc_taup_seconds=PKPbc_taup-starttime
        PKPab_taup_seconds=PKPab_taup-starttime
        PKPbc_diff_taup=PKPbc_taup-PKIKP_taup
        PKPab_diff_taup=PKPab_taup-PKIKP_taup
        cut1=PKIKP_taup-20
        cut2=PKPab_taup+20 
        PKPab_switch=True
        PKPbc_switch=True
    
    #PKPbc and PKIKP
    if PKPbc_switch and arrivaltimelength==2 and (arrivals[PKIKP_index].purist_name=="PKIKP") and not PKPab_switch :
        PKIKP_taup=starttime+arrivals[PKIKP_index].time
        PKPbc_taup=starttime+arrivals[PKPbc_index].time
        PKIKP_taup_seconds=PKIKP_taup-starttime
        PKPbc_taup_seconds=PKPbc_taup-starttime
        PKPab_taup_seconds=False
        PKPbc_diff_taup=PKPbc_taup-PKIKP_taup
        PKPab_diff_taup=False  
        PKPab_taup=False
        cut1=PKIKP_taup-20
        cut2=cut1+40
    
    #PKPab and PKIKP
    if PKPab_switch and arrivaltimelength==2 and (arrivals[PKIKP_index].purist_name=="PKIKP")and not PKPbc_switch :
        PKIKP_taup=starttime+arrivals[PKIKP_index].time
        PKPab_taup=starttime+arrivals[PKPab_index].time
        PKPab_taup_seconds=PKPab_taup-starttime
        PKIKP_taup_seconds=PKIKP_taup-starttime        
        PKPbc_taup_seconds=False
        PKPab_diff_taup=PKPab_taup-PKIKP_taup
        PKPbc_taup=False
        PKPbc_diff_taup=False   
        cut1=PKIKP_taup-20
        cut2=PKPab_taup+20
    #Only PKIKP
    if arrivaltimelength==1 and (arrivals[0].purist_name=="PKIKP"):
        PKIKP_taup=starttime+arrivals[0].time
        PKIKP_taup_seconds=PKIKP_taup-starttime        
        PKPab_taup_seconds=False
        PKPab_taup=False
        PKPab_diff_taup=False   
        PKPbc_taup_seconds=False
        PKPbc_taup=False
        PKPbc_diff_taup=False   
        cut1=PKIKP_taup-20
        cut2=PKIKP_taup+40

    #Window data depending on presence of PKPbc or PKPab:
    trace_cut=trace_filt.trim(starttime=cut1,endtime=cut2,pad=False,nearest_sample=True,fill_value=None)
    
    #####################################################################################
    # Pierce Point and Turning Point Calculation
    ##################################################################################### 
    pierce=arrivals[PKIKP_index].pierce
    z_len=len(pierce[:])
    for z in range (0,z_len):
        pierce_test=pierce[z][3]
        if pierce_test > 5154 : #In AK135 Inner core is at 5153.5km depth
            z_save=z    #Find location of Turning point
    InnerCoreTime=pierce[z_save+1][1]-pierce[z_save-1][1]
    
    #Calculate Turning point lat,long,azimuth and distance:
    TurningPoint=pierce[z_save]                 #Turning Point 
    TurningPoint_dist=TurningPoint[2]*(180/pi)  #Distance between Event and Turning point
    TurningPoint_depth=TurningPoint[3]          #Depth of Turning point
    
    #Calc lat and long of turning point
    tuloc=Geodesic.WGS84.ArcDirect(lat1=Evlat,lon1=Evlon,azi1=azimuth,a12=TurningPoint_dist,outmask=1929)
    tulat=tuloc['lat2']
    tulon=tuloc['lon2']

    #Get Distance of pierce points and then calculate the lat and lon of pierce points
    indist=pierce[z_save-1]
    indist=indist[2]*(180/pi)
    outdist=pierce[z_save+1]
    outdist=outdist[2]*(180/pi)

    #Calc lat and long of in-pierce point
    inloc=Geodesic.WGS84.ArcDirect(lat1=Evlat,lon1=Evlon,azi1=azimuth,a12=indist,outmask=1929)
    inlat=inloc['lat2']
    inlon=inloc['lon2']

    #Calc lat and long of out-pierce point
    outloc=Geodesic.WGS84.ArcDirect(lat1=Evlat,lon1=Evlon,azi1=azimuth,a12=outdist,outmask=1929)
    outlat=outloc['lat2']
    outlon=outloc['lon2']
    
    #Calculate Zeta: Last resort: if Zeta > 35 force DataProcess_All to put data in Bad data folder
    #zeta=ZetaCalculate(inlat,inlon,outlat,outlon)
    #if zeta > 35:
    #    return(results,columns)

    #####################################################################################
    # Pick Arrivals
    #####################################################################################
    print("############################")
    print(network,station,EventtimeUSGS.year,EventtimeUSGS.julday)
    print(EventtimeUSGS)
    print("Distance: ",round(dist,0),"Mag: ",Magnitude)
    print("Depth: ",depth)
    print("Tulon: ",tulon)
    print("############################")

    #Okay so this is a bit difficult to completely explain but this basically will open up an interactive figure where you shall then pick data!
    #There is a lot going on but you should pick the onset of a phase, the peak and then at the end hit the numbers 1 or 2

    #Setup Figure:
    fig = plt.figure(figsize=(25,5.5))  #Figure size, in inches.
    ax = fig.add_subplot(111)
    ax.plot(trace_cut.times(),trace_cut.data, "-b", lw=1)
    ax.axvline(x=PKIKP_taup-cut1,color='r',label="PKIKP (AK135)",linestyle='--')
    if PKPbc_switch :
        ax.axvline(x=PKPbc_taup-cut1,color='green',label="PKPbc (AK135)",linestyle='--')
    if PKPab_switch :
        ax.axvline(x=PKPab_taup-cut1,color='purple',label="PKPab (AK135)",linestyle='--')
    if os.path.isfile("Results.xlsx"):
        ax.legend()            
    ax.set_xlim(-5)

    #Setup for Manual Picking:
    manual_picks=[]
    trigger=[]
    Quality_number=[]
    Quality_number.append(3)
    rightbutton=[]
    clicknum=[]
    clicknum=0
    def onclick(event):
        global ix
        ix=event.xdata
        manual_picks.append(ix)
        print(len(manual_picks))
        if len(manual_picks) == 6:
            print("Arrivals Picked. Provide QN (1 or 2):")
    
    def on_key(event):
        if event.key==" ":
            manual_picks.clear()
            manual_picks.append(-1)
            manual_picks.append(-1)
            manual_picks.append(-1)
            manual_picks.append(-1)
            manual_picks.append(-1)
            manual_picks.append(-1)
            fig.canvas.mpl_disconnect(cid)
            plt.close()                

        if event.key=="1":
            Quality_number.clear()
            Quality_number.append(1)
            fig.canvas.mpl_disconnect(cid)
            plt.close()
        if event.key=="2":
            Quality_number.clear()
            Quality_number.append(2)            
            fig.canvas.mpl_disconnect(cid)
            plt.close()       
        if event.key=="escape":
                print("Exit")
                sys.exit()
        if (not event.key ==" ") and (not event.key =="1") and(not event.key =="2"):
            print("Invalid Key Press. Try again.")
    
    cid = fig.canvas.mpl_connect('button_press_event', onclick)
    cid = fig.canvas.mpl_connect('key_press_event', on_key)
    plt.show()
    print(manual_picks)
    #Organise Input
    Quality_number=Quality_number[0]
    
    def Beez(PKIKP,PKP,cut1,starttime,InnerCoreTime,diff_taup):
            PKP=cut1+PKP
            PKP_seconds=PKP-starttime
            PKP_diff=PKP-PKIKP
            dt_residual=PKP_diff-diff_taup
            frac_dt=dt_residual/InnerCoreTime       
            return PKP,PKP_seconds,dt_residual,frac_dt

    #Organise Picks
    if manual_picks[0]> 0 or manual_picks[1] > 0 :
        if manual_picks[0]>0:
            #Onset PKIKP:
            PKIKP_manual_onset=cut1+manual_picks[0]
            PKIKP_manual_seconds_onset=PKIKP_manual_onset-starttime
        else:
            PKIKP_manual_onset,PKIKP_manual_seconds_onset=("DataNotGood",)*2
        
        if manual_picks[1]>0:
            #Peak PKIKP:
            PKIKP_manual_peak=cut1+manual_picks[1]
            PKIKP_manual_seconds_peak=PKIKP_manual_peak-starttime
        else:
            PKIKP_manual_peak,PKIKP_manual_seconds_peak=("DataNotGood",)*2

        #Onset PKPbc
        if manual_picks[2]>0 and manual_picks[0]> 0 :
            PKPbc_manual_onset,PKPbc_manual_onset_seconds,dt_residual_bc_onset,frac_dt_bc_onset=Beez(PKIKP_manual_onset,manual_picks[2],cut1,starttime,InnerCoreTime,PKPbc_diff_taup)
        else:
            PKPbc_manual_onset,PKPbc_manual_onset_seconds,dt_residual_bc_onset,frac_dt_bc_onset=("DataNotGood",)*4
            
        #Peak PKPbc
        if manual_picks[3]>0 and manual_picks[1]> 0 :
            PKPbc_manual_peak,PKPbc_manual_peak_seconds,dt_residual_bc_peak,frac_dt_bc_peak=Beez(PKIKP_manual_peak,manual_picks[3],cut1,starttime,InnerCoreTime,PKPbc_diff_taup)
        else:
            PKPbc_manual_peak,PKPbc_manual_peak_seconds,dt_residual_bc_peak,frac_dt_bc_peak=("DataNotGood",)*4
            
        #Onset PKPab
        if manual_picks[4]>0 and manual_picks[0]> 0 :
            PKPab_manual_onset,PKPab_manual_onset_seconds,dt_residual_ab_onset,frac_dt_ab_onset=Beez(PKIKP_manual_onset,manual_picks[4],cut1,starttime,InnerCoreTime,PKPab_diff_taup)
        else:
            PKPab_manual_onset,PKPab_manual_onset_seconds,dt_residual_ab_onset,frac_dt_ab_onset=("DataNotGood",)*4
            
        #Peak PKPab
        if manual_picks[5]>0 and manual_picks[1]> 0 :
            PKPab_manual_peak,PKPab_manual_peak_seconds,dt_residual_ab_peak,frac_dt_ab_peak=Beez(PKIKP_manual_peak,manual_picks[5],cut1,starttime,InnerCoreTime,PKPab_diff_taup)
        else:
            PKPab_manual_peak,PKPab_manual_peak_seconds,dt_residual_ab_peak,frac_dt_ab_peak=("DataNotGood",)*4
    
        switch2_plot=True    
        Data_Keep=1
        DataIsGood=True
    else:
        PKPbc_manual_onset,PKPbc_manual_onset_seconds,dt_residual_bc_onset,frac_dt_bc_onset=("DataNotGood",)*4
        PKPbc_manual_peak,PKPbc_manual_peak_seconds,dt_residual_bc_peak,frac_dt_bc_peak=("DataNotGood",)*4
        PKPab_manual_peak,PKPab_manual_peak_seconds,dt_residual_ab_peak,frac_dt_ab_peak=("DataNotGood",)*4
        PKPab_manual_onset,PKPab_manual_onset_seconds,dt_residual_ab_onset,frac_dt_ab_onset=("DataNotGood",)*4
        PKIKP_manual_onset,PKIKP_manual_seconds_onset,PKIKP_manual_peak,PKIKP_manual_seconds_peak=("DataNotGood",)*4
        Data_Keep=0
        DataIsGood=False
        Quality_number=3

    #####################################################################################
    # Organise Picks and Write Results
    #####################################################################################
    #Get EventID number
    cat=obspy.read_events("EventInfo.xml")
    cat_select=cat.events[0]
    resourceidstring=str(cat_select.resource_id)
    id_index=resourceidstring.find("eventid=")
    eventid=resourceidstring[id_index+8:]
    eventid=eventid[0:10]
    #Folder name
    Foldername=os.path.basename(os.getcwd())

    #######################
    #Write Results to a spreadsheet:
    columns =["DataKeep",            
        "DataIsGood",          
        "QualityNumber",       
        "Foldername",
        "Station",
        "Network",
        "Year",
        "JulianDay",
        "EventTime",
        "UTCEventTime",
        "Stalat",
        "Stalon",
        "Evlat",
        "Evlon",
        "EventDepth",
        "EventMag",
        "EventID",
        "Distance",
        "Azimuth",
        'Zeta',
        "PKPbc_switch",
        "PKPab_switch",
        "PKIKP",
        "PKPbc",
        "PKPab",
        "PKIKP_peak",
        "PKPbc_peak",
        "PKPab_peak",
        "PKIKP_taup",
        "PKPbc_taup",
        "PKPab_taup",
        "Inlat",
        "Inlon",
        "Tulat",
        "Tulon",
        "Outlat",
        "Outlon",
        "TPDist",
        "TPDepth",
        "dt_residual_bc",
        "dt_residual_ab",
        "dt_t_bc",
        "dt_t_ab",
        "InnerCoreTime"]

    results=[Data_Keep,
            DataIsGood,
            Quality_number,
            Foldername,
            station,
            network,
            starttime.year,
            starttime.julday,
            str(starttime.hour)+":"+str(starttime.minute)+":"+str(starttime.second),
            starttime,
            stalat,
            stalon,
            Evlat,
            Evlon,
            depth,
            Magnitude,
            eventid,
            dist,
            azimuth,
            zeta,
            PKPbc_switch,
            PKPab_switch,
            PKIKP_manual_seconds_onset,
            PKPbc_manual_onset_seconds,
            PKPab_manual_onset_seconds,
            PKIKP_manual_seconds_peak,
            PKPbc_manual_peak_seconds,
            PKPab_manual_peak_seconds,
            PKIKP_taup_seconds,
            PKPbc_taup_seconds,
            PKPab_taup_seconds,
            inlat,
            inlon,
            tulat,
            tulon,
            outlat,
            outlon,
            TurningPoint_dist,
            TurningPoint_depth,
            dt_residual_bc_onset,
            dt_residual_ab_onset,
            frac_dt_bc_onset,
            frac_dt_ab_onset,
            InnerCoreTime]

    #Write Spreadsheet: 
    df1=pd.DataFrame([results],columns=columns)
    filename ='Results.xlsx'
    writer = pd.ExcelWriter(filename)
    df1.to_excel(writer,"Sheet1")
    writer.save()
    #-----------------------------------#
    return(results,columns)

#If running the script as a standalone then run function Freyr()
if __name__ == '__main__':
    print("################################################################################################")
    print("This is a python script to calculate differential arrival times of PKIKP and its phases and zeta")
    Data_Process()
