############################################################################################
########################   BODY WAVES MODULE: Results Gather   ############################
#############################################################################################
#Henry Brett, Utrecht University, 2018,2019,2020
#This is a code which will go through all the data in the good folder
#and get results from the excel spreadsheets
import glob,os,sys
import pandas as pd
#Personal Modules:
import matplotlib.pyplot as plt
import Data_Process
from Data_Read import ReadDataStationEvent_function

#------------------Define Directories------------------#
CodesDirectory=sys.path[0]
EnvironmentDirectory=os.path.dirname(CodesDirectory)
DataDirectory=EnvironmentDirectory+"/Data"
ResultsDirectory=EnvironmentDirectory+"/Results"
#------------------Define Directories------------------#

path=DataDirectory+"/Good/"
path=DataDirectory+"/Good/*"
foldernames=glob.glob(path)
foldernames=sorted(foldernames)
i_end=len(foldernames)
Zeta=[]
dt=[]
stalat=[]
first_row=True
for i in range(0,i_end) :
    print("####################################################################")
    print("Indice:",i,"Data Left:",i_end-1-i)
    os.chdir(foldernames[i])
    filename="Results.xlsx"
    dataframe_temp=pd.read_excel(filename, sheet_name="Sheet1")
    if first_row and dataframe_temp.PKPbc.values[0] != 'DataNotGood':
        dataframe=dataframe_temp.copy()
        first_row=False
    else: 
        if dataframe_temp.PKPbc.values[0] != 'DataNotGood':
            dataframe=dataframe.append(dataframe_temp,ignore_index=True)
os.chdir(CodesDirectory)
#Write out collected Excel spreadsheet:
filename=ResultsDirectory+'/CollectedResults.xlsx'
writer=pd.ExcelWriter(filename)
dataframe.to_excel(writer,'Sheet1')
writer.save()
print("Looping over Data has completed")

#Example of how to read in an entire column:
filename=ResultsDirectory+'/CollectedResults.xlsx'
results=pd.read_excel(filename, sheet_name="Sheet1")
dt=results.dt_t_bc.values[:]
Zeta=results.Zeta.values[:]

#Example of how to separate a dataframe:
#data_henry=results.loc[results.Year <2020]
#data_sandra=results.loc[results.Year>=2020]

#Find Outliers:
data_outliers=results.loc[(results.Tulon < 155) & (results.Tulon > 155) & (results.dt_t_bc > 0.01)]
print(data_outliers.Foldernames)


fig=plt.figure(figsize=(5,5))
plt.title('dt versus Zeta')
sc=plt.scatter(dt,Zeta,c=Zeta,cmap='coolwarm',vmin=0,vmax=90)
plt.xticks([0,0.01,0.02])
plt.colorbar(sc,label='My first colorbar')
plt.xlabel('dt/t')
plt.ylabel('Zeta')
plt.savefig(ResultsDirectory+'/dt_zeta_bc.pdf')
