#############################################################################################
#########################   BODY WAVES MODULE: DATA DOWNLOAD   ##############################
#############################################################################################
#Henry Brett, Utrecht University, 2018,2019,2020
#This is a code to download data from the FDSN and place in an unevaluated folder. then process 
#the data and see if it is of interest, if not: delete. if so: place in the approporiate unevaluated folder.

### Import Packages ###
import obspy
from obspy import read, UTCDateTime, read_inventory, Stream, Trace
from obspy.clients.fdsn.mass_downloader import CircularDomain,Restrictions, MassDownloader,RectangularDomain
from obspy.clients.fdsn import Client
from obspy.taup import TauPyModel
from obspy.geodetics import locations2degrees,gps2dist_azimuth
import datetime
import glob
import sys, os
import pandas as pd
#Personal Modules:

#------------------Define Directories------------------#
CodesDirectory=sys.path[0]
EnvironmentDirectory=os.path.dirname(CodesDirectory)
DataDirectory=EnvironmentDirectory+"/Data"
ResultsDirectory=EnvironmentDirectory+"/Results"
#------------------Define Directories------------------#

###########################################################################
#   FUNCTIONS:
############################################################################
def DataDownloader(cat,NorthernHemisphere):
    path_temp=DataDirectory+"/Unevaluated/Temp"
    cat_len=len(cat)
    for i in range (0,cat_len): #cat_len):  #For one Event
        #try :   #This basically says that if an error comes up, go to the next earthquake
        #######################
        # Earthquake Information
        ########################
        print("################################################")
        print('Collecting Data: ',i,"out of",cat_len)
        cat_select=cat.events[i]                    #Get information of one single Earthquake
        EventtimeUSGS=cat_select.origins[0].time    #Get the Origin time of the Earthquake
        Evlon=cat_select.origins[0].longitude       #Get Earthquake Longitude
        Evlat=cat_select.origins[0].latitude        #Get Earthquake Latitude
        EvDepth=cat_select.origins[0].depth         #Get Earthquake Depth
        os.makedirs(DataDirectory+"/Unevaluated/Temp",exist_ok=True)    #Create Temp folder where data is downloaded initially before processing
        #Only download data from seismic stations withing 146.5 degrees and 180 Degrees from the EQ, as this is the epicentral distance from which you can observe PKPbc and PKPab phases
        
        #domain = CircularDomain(latitude=Evlat, longitude=Evlon,minradius=146.5, maxradius=180) 
        ################################################
        if NorthernHemisphere:
            domain = RectangularDomain(maxlatitude=-60)
        else:
            domain = RectangularDomain(maxlatitude=60)
        ################################################

        #Clean up from Previous Collection 
        if len(os.listdir(path_temp))>0:
            print("Temp folder still contains files")
            print("Indice:",i)
            files = glob.glob(path_temp+'/*')
            for f in files:
                print('Deleting',len(files))
                os.remove(f)

        ##############
        #Data Request
        #############
        origin_time = EventtimeUSGS #The time the EQ occured 
        #-------------------Station Restrictions-------------------#
        restrictions = Restrictions(
            # This defines the temporal bounds of the waveform data.
            starttime=origin_time,
            endtime=origin_time + 3600, #Collect the seismogram from the time the EQ occured + 3600seconds
            reject_channels_with_gaps=True, #Don't bother with patchy data
            
            # And you might only want waveforms that have data for at least 95 % of
            # the requested time span. Any trace that is shorter than 95 % of the
            # desired total duration will be discarded.
            minimum_length=0.95,

            # No two stations should be closer than 10km to each other. This is
            # useful to for example filter out stations that are part of different
            # networks but at the same physical station. Settings this option to
            # zero or None will disable that filtering.
            minimum_interstation_distance_in_m=10E3,
            
            # Only HH or BH Vertical channels. If a station has HH channels, those will be
            # downloaded, otherwise the BH. Nothing will be downloaded if it has
            # neither. You can add more/less patterns if you like.
            channel_priorities=["HH[Z]", "BH[Z]"],
            
            # Location codes are arbitrary and there is no rule as to which
            # location is best. Same logic as for the previous setting.
            location_priorities=["", "00", "10"],
            
            #Make sure there is a station.xml file
            sanitize=True,   
            exclude_stations=["PURD"])  #This station is a pain. If you find other seismic stations which break the data downloader you can add them to this list
        #-------------------Station Restrictions-------------------#
        
        # No specified providers will result in all known ones being queried.
        mdl = MassDownloader(providers=["IRIS"])    #For now just use IRIS
        mdl.download(domain, restrictions, mseed_storage=path_temp,stationxml_storage=path_temp) #This starts the download
        
        #After Download to temp folder start processing downloaded data
        os.chdir(path_temp) #Changes directory to Data/Unevaluated/temp
        filenames=glob.glob(path_temp+"/*.mseed")
        filenames=sorted(filenames)
        
        #Get parameters for defining the foldername: station_year_day_hour_minute
        year=cat_select.origins[0].time.year
        day=cat_select.origins[0].time.julday
        hour=cat_select.origins[0].time.hour
        minute=cat_select.origins[0].time.minute
        filenames_len=len(filenames)
        for x in range (0,filenames_len) :
            Data=read(filenames[x])
            station=Data[0].stats.station
            network=Data[0].stats.network
            foldername=station+" "+str(year)+" "+str(day)+" "+str(hour)+":"+str(minute)
            stationxmlname=network+"."+station+".xml"

            print("#------------------------------------#")
            print(foldername,' ',str(x),'/',str(filenames_len))
            #Check if already exists or has been inspected:
            path1=DataDirectory+"/Unevaluated/"+foldername
            path2=DataDirectory+"/Good/"+foldername
            path3=DataDirectory+"/Bad/"+foldername
            
            switch_exist1=os.path.isdir(path1)
            switch_exist2=os.path.isdir(path2)
            switch_exist3=os.path.isdir(path3)
            
            if switch_exist1 or switch_exist2 or switch_exist3: #If data has already been downloaded, or inspected (ie. placed in 'Good' or 'Bad' folder) then delete.
                print("Data Already Exists, delete.")
                os.remove(filenames[x])
                os.remove(path_temp+"/"+stationxmlname)
            else :  #Otherwise create new directory and move all the data there
                UnevaluatedPath=DataDirectory+"/Unevaluated/"+foldername
                os.mkdir(UnevaluatedPath)
                mseedname=os.path.basename(filenames[x])
                os.rename(filenames[x],UnevaluatedPath+"/"+mseedname)
                os.rename(stationxmlname,UnevaluatedPath+"/"+stationxmlname)
                cat_select.write(UnevaluatedPath+"/EventInfo.xml",format="QUAKEML")
                #Go back to original unevaluated path and do again for next EQ 
        #Check if Temp Folder is not empty
        if len(os.listdir(path_temp))>0:
            files = glob.glob(path_temp+'/*')
            for f in files:
                os.remove(f)
        
        #except(obspy.clients.fdsn.header.FDSNNoDataException,FileNotFoundError,ValueError,NotImplementedError,obspy.io.mseed.InternalMSEEDError) :
        #    print("There was an exception and thus data was not downloaded")
        #    print('Skip this data and go to the next one')
        #    print("Indice:",i)
        #    continue

###########################################################################
#   DATA DOWNLOAD START:
############################################################################

print(" ")
print("Collect Event Information:")
# 1. Create Temp folder
# 2. Name Cat file
# 3. Rename Index file and clear
# 4. Re-do lat and lon of stations
# 5. Re-do lat and lon of Events

#Retrieve event information and write catfile
#search for EQ's between t1 and t2
t1 = UTCDateTime(year=2020, julday=118,hour=00)  #1st of February 2020, julian day: 1 = 1/1, 365= 31/12   
t2 = UTCDateTime(year=2020, julday=119,hour=23)  #2nd of February 2020   
client = Client("USGS")


########################
#Northern Hemisphere:
NorthernHemisphere=False
if NorthernHemisphere:
    cat=client.get_events(starttime=t1, endtime=t2,minmagnitude=5.3,minlatitude=60) #Query data centre for EQ information
else:
    #Southern Hemisphere:
    cat=client.get_events(starttime=t1, endtime=t2,minmagnitude=5.3,maxlatitude=-60) #Query data centre for EQ information
#########################
#Catfile = catalogue of EarthQuakeInfo.xml:
catfile=DataDirectory+'/EarthQuakeInfo.xml'
cat.write(catfile,format="QUAKEML")

#Download Data
cat=obspy.core.event.catalog.read_events(catfile) #The EQ information file written above
DataDownloader(cat,NorthernHemisphere)  #Function defined above, given a catalogue of earthquake information download the data
