############################################################################################
########################   BODY WAVES MODULE: DATA PROCESS ALL   ############################
#############################################################################################
#Henry Brett, Utrecht University, 2018,2019,2020
#This is a code which will go through multiple data folders in Unevaluated...
#and run the Data_process.py function in each one. Then afterwards analyse the results and place
#either in the 'Good' data folder or the 'Bad' data folder.
import glob,os,sys
#Personal Modules:
import Data_Process
from Data_Read import ReadDataStationEvent_function

#------------------Define Directories------------------#
CodesDirectory=sys.path[0]
EnvironmentDirectory=os.path.dirname(CodesDirectory)
DataDirectory=EnvironmentDirectory+"/Data"
ResultsDirectory=EnvironmentDirectory+"/Results"
#------------------Define Directories------------------#

path=DataDirectory+"/Unevaluated/"
path=DataDirectory+"/Unevaluated/*"
foldernames=glob.glob(path)
foldernames=sorted(foldernames)
i_end=len(foldernames)
for i in range(0,i_end) :
    print("####################################################################")
    print("Indice:",i,"Data Left:",i_end-1-i)
    os.chdir(foldernames[(i)])
    output=Data_Process.Data_Process()  #Run function contained in /Codes/Data_Process.py
    results=output[0]
    columns=output[1]
    #Now organise data into correct directory depending on outcome of picking
    os.chdir(DataDirectory)
    if results[1] :
        print("Moving Data to good data folder")
        newpath=DataDirectory+"/Good/"+str(os.path.basename(foldernames[i]))
        os.rename(foldernames[i],newpath)
    if not results[1] :
        print("Moving Data to bad data folder")
        newpath=DataDirectory+"/Bad/"+str(os.path.basename(foldernames[i]))
        os.rename(foldernames[i],newpath)
print("Looping over Data has completed")
